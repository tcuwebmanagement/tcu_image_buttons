<?php
/**
 *  Plugin Name:    TCU Image Button Widget
 *  Description:    This plugin gives you the ability to add a special button styling. You will be able to add an image, text for the link, and the URL.
 * Version:        1.4
 * Author:         Website & Social Media Management
 *  Author URI:     http://mkc.tcu.edu/web-management.asp
 *
 * @package tcu_image_button
 * @since TCU Image Button 1.0.0
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * INCLUDE NEEDED FILES
 */
require_once 'classes/tcu_image_button_class.php';

/**
 *  INITIATE PLUGIN
 */
function tcu_image_button_initiate() {
	flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'tcu_image_button_initiate' );

/**
 * DEACTIVATE PLUGIN
 */
function tcu_image_button_deactivate() {
	flush_rewrite_rules();
}
register_deactivation_hook( __FILE__, 'tcu_image_button_deactivate' );


