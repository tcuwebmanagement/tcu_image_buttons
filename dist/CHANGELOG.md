## TCU Image Button

Use the TCU Image Button Plugin adds a special widget that allows the user to add a background image to a button area. This plugin must include TCU Web Standards theme.

---

**v1.4**

*   Fix PHPCS, ESLint, and ScssLint errors

**v1.3**

*   Added aria-label to links
*   Cleaned up some PHP code

**v1.2 update**

*   inlined frontend styles
*   inlined backend styles

**v1.1 update**

*   changed the text within the media uploader button to say "Inser into Widget"
*   Hide upload button when image is selected and adds remove link
