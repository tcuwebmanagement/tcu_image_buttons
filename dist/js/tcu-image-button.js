/**
 * Media Upload
 *
 */
jQuery( document ).ready( function( $ ) {

    // variables
    var fileFrame;

    $( document ).on( 'click', '.tcu-image-button-upload', function( e ) {
        e.preventDefault();

        var button = $( this );
        var inputId = $( this )
            .parent( '.tcu-image-widget' )
            .find( '.tcu-image-button-url' )
            .attr( 'id' );
        var imgContainer = $( this ).parent( '.tcu-image-widget' );
        var heightId = $( this )
            .parents( 'form' )
            .find( '.tcu-image-height' )
            .attr( 'id' );
        var widthId = $( this )
            .parents( 'form' )
            .find( '.tcu-image-width' )
            .attr( 'id' );

        // crate a new media fileFrame
        fileFrame = wp.media( {
            title: 'Select Image',
            button: {
                text: 'Insert into Widget'
            },
            multiple: false // Set to true to allow multiple files to be selected
        } );

        fileFrame.on( 'select', function() {
            var attachment = fileFrame
                .state()
                .get( 'selection' )
                .first()
                .toJSON();

            // Send the attachment URL to our custom image input field
            imgContainer.prepend(
                '<img style="max-width: 100%; height: auto;" src="' +
                    attachment.url +
                    '" class="tcu-custom-image" /><br><a class="tcu-image-button-clear-image" style="font-size: 10px;" href="#">Remove Image</a>'
            );

            $( '#' + inputId ).val( attachment.url );

            // hide upload button
            button.css( 'display', 'none' );

            // populate image size and add into our form
            $( '#' + widthId ).val( attachment.width );
            $( '#' + heightId ).val( attachment.height );
        } );

        // open the modal on click
        fileFrame.open();
    } );

    // empty values for image_url, width, and height
    $( document ).on( 'click', '.tcu-image-button-clear-image', function( e ) {
        e.preventDefault();

        var removeLink = $( this );
        var inputId = $( this )
            .parent( '.tcu-image-widget' )
            .find( '.tcu-image-button-url' )
            .attr( 'id' );
        var imgContainer = $( this ).parent( '.tcu-image-widget' );
        var image = $( this )
            .parent( '.tcu-image-widget' )
            .find( '.tcu-custom-image' );
        var heightId = $( this )
            .parents( 'form' )
            .find( '.tcu-image-height' )
            .attr( 'id' );
        var widthId = $( this )
            .parents( 'form' )
            .find( '.tcu-image-width' )
            .attr( 'id' );

        image.remove();
        imgContainer.prepend(
            '<button type="button" style="margin: 0 auto;" class="tcu-image-button-upload button-primary">Upload Image</button>'
        );
        removeLink.css( 'display', 'none' );
        $( '#' + inputId ).val( '' );
        $( '#' + heightId ).val( '' );
        $( '#' + widthId ).val( '' );
    } );
} );
