<?php
/**
 * Widget that creates a custom button
 * to use in any TCU theme
 *
 * @subpackage tcu_image_button_class
 * @since TCU Image Button 1.0.0
 */

// Load our widget.
add_action( 'widgets_init', function() {
	register_widget( 'TCU_Image_Button' );
});

/**
 * Adds our widget
 */
class TCU_Image_Button extends WP_Widget {
	/**
	 * Register widget with WordPress
	 */
	public function __construct() {
		parent::__construct(
				'tcu_image_button', // Base ID
				__('Image Button'), // Name
				array( 'description' => __('Add a button with an image as a background') ) // Args
			);

		add_action( 'sidebar_admin_setup', array( $this, 'upload_scripts' ) );
	}

	/**
	 * Enqueue all the javascript.
	 */
	public function upload_scripts() {
		wp_enqueue_media();
		wp_enqueue_script( 'tcu-image-button', plugins_url('js/tcu-image-button.js', dirname( __FILE__ ) ), array( 'jquery', 'media-upload', 'media-views' ), '1.3', true );
	}

	/**
	 * Front-end display of widget
	 * @see WP_Widget::widget()
	 *
	 * @param array $args Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance  ) {
		global $post;

		// these are the widget options
		$image_url  = $instance['image_url'];
		$url        = $instance['url'];
		$link_text  = $instance['link_text'];
		$aria_label = $instance['aria_label'];
		$target     = $instance['target'];
		$align      = $instance['align'];
		$height     = $instance['height'];
		$width      = $instance['width'];


		// Before widget (defined by themes).
		echo wp_kses_post( $args['before_widget'] );
		?>
			<style type="text/css">
			.button-home {
				z-index: 3;
			}
			.button-home:hover {
				color: #FFFFFF;
				background-color: #777777;
				text-decoration: none;
			}
			.btn-image {
				background-size: cover;
				width: 100%;
				padding: 0;
				position: relative;
				overflow: hidden;
				display: flex;
				justify-content: center;
				align-items: center;
			}
			.btn:hover .btn-image__hover {
				opacity: 1;
				transform: scale( 1, 1);
			}
			.btn-image__hover {
				background-color: rgba(77, 25, 121, 0.4);
				width: 100%;
				height: 100%;
				position: absolute;
				top: 0;
				left: 0;
				opacity:0;
				transform: scale(0, 0);
				transition: all 0.4s ease-out 0.3s;
				z-index: 0;
			}
			</style>

			<div style="background: transparent url(<?php echo esc_url($image_url); ?>) top center no-repeat; height: <?php echo $height; ?>px; max-width: <?php echo $width; ?>px; <?php echo $this->align_image( $instance ); ?>" class="btn btn-image">
				<div class="btn-image__hover"></div>
				<a <?php if ( $aria_label ) { echo 'aria-label="' . esc_attr( $aria_label ) . '"'; } ?> target="<?php echo esc_attr($this->link_target( $instance )); ?>" href="<?php echo esc_url($url); ?>" class="tcu-button tcu-button--primary button-home"><?php echo esc_html($link_text); ?></a>
			</div>

		<?php
		// After widget (defined by themes).
		echo wp_kses_post( $args['after_widget'] );
	}

	/**
	 * Form UI
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		$image_url  = ( $instance ) ? $instance['image_url'] : '';
		$url        = ( $instance ) ? $instance['url'] : '';
		$link_text  = ( $instance ) ? $instance['link_text'] : '';
		$aria_label = ( $instance ) ? $instance['aria_label'] : '';
		$target     = ( $instance ) ? $instance['target'] : '';
		$align      = ( $instance ) ? $instance['align'] : '';
		$height     = ( $instance ) ? $instance['height'] : '';
		$width      = ( $instance ) ? $instance['width'] : '';
		$open       = '<p>';
		$closing    = '</p>';
		?>

		<div style="margin: 2em auto; max-width: 100%;" class="tcu-image-button-container">

			<div style="text-align: center; margin-top: 1em; margin-bottom: 1em;" class="tcu-image-widget">

				<?php
				if( $image_url ) {
					echo '<img style="max-width: 100%; height: auto;" class="tcu-custom-image" src="'.$image_url.'" /><br>
					<a style="font-size: 10px;" class="tcu-image-button-clear-image" href="#">Remove Image</a>';
				} else {
					echo '<button type="button" style="margin: 0 auto;" class="tcu-image-button-upload button-primary">Upload Image</button>';
				}
				echo $this->create_input( 'hidden', $this->get_field_id('image_url'), $this->get_field_name('image_url'), $image_url, 'tcu-image-button-url');
				?>

			</div><!-- end of .tcu-image-widget -->

			<?php
			// Link.
			echo $open;
			echo $this->create_label( $this->get_field_id( 'url' ), 'Link:' );
			echo $this->create_input( 'text', $this->get_field_id( 'url' ), $this->get_field_name( 'url' ), $url );
			echo $closing;

			// Link Text.
			echo $open;
			echo $this->create_label( $this->get_field_id( 'link_text' ), 'Link Text:' );
			echo $this->create_input( 'text', $this->get_field_id( 'link_text' ), $this->get_field_name( 'link_text' ), $link_text );
			echo $closing;

			// Aria-Label.
			echo $open;
			echo $this->create_label( $this->get_field_id( 'aria_label' ), 'Aria-label:' );
			echo $this->create_input( 'text', $this->get_field_id( 'aria_label' ), $this->get_field_name( 'aria_label' ), $aria_label );
			echo $closing;
			?>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id('align') ); ?>"><?php esc_html_e( 'Align Image:', 'tcu_image_buttons' ); ?></label>
				<select name="<?php echo esc_attr( $this->get_field_name('align') ); ?>" class="widefat">
					<?php
						$align_options = array('none', 'left', 'center', 'right');
						foreach( $align_options as $option ) {
							echo '<option value="' . esc_attr( $option ) . '" id="' . esc_attr( $option ) . '"', $align == $option ?  'selected="selected"' : '', '>', esc_html_e( $option ), '</option>';
						}
					?>
				</select>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'target') ); ?>"><?php echo esc_html_e( 'Link Target', 'tcu_image_buttons' ); ?></label>
				<select name="<?php echo esc_attr( $this->get_field_name( 'target' ) ); ?>" class="widefat">
					<?php
						$options = array('Stay in window', 'Open in new window');
						foreach( $options as $option ) {
							echo '<option value="' . esc_attr( $option ) . '" id="' . esc_attr( $option ) . '"', $target == $option ?  'selected="selected"' : '', '>', esc_html_e( $option ), '</option>';
						}
					?>
				</select>
			</p>

			<?php
			// Height.
			echo $open;
			echo $this->create_label( $this->get_field_id( 'height' ), 'Height:' );
			echo $this->create_input( 'text', $this->get_field_id( 'height' ), $this->get_field_name( 'height' ), $height, 'widefat tcu-image-height' );
			echo $closing;

			// Height.
			echo $open;
			echo $this->create_label( $this->get_field_id( 'width' ), 'Width:' );
			echo $this->create_input( 'text', $this->get_field_id( 'width' ), $this->get_field_name( 'width' ), $width, 'widefat tcu-image-width' );
			echo $closing;
			?>

		</div><!-- end of .tcu-image-button-container -->
	<?php }

	/**
	 * Sanitize widget form values as they are saved
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved
	 * @param array $old_instance Previously saved values from database
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;
		$instance['image_url']  = strip_tags( $new_instance['image_url'] );
		$instance['url']        = strip_tags( $new_instance['url'] );
		$instance['link_text']  = strip_tags( $new_instance['link_text'] );
		$instance['aria_label'] = strip_tags( $new_instance['aria_label'] );
		$instance['align']      = strip_tags( $new_instance['align'] );
		$instance['target']     = strip_tags( $new_instance['target'] );
		$instance['height']     = strip_tags( $new_instance['height'] );
		$instance['width']      = strip_tags( $new_instance['width'] );
		return $instance;

	}

	/**
	 * Returns correct string to use for aligning our image
	 *
	 * @param array $instance
	 * @return string Updated string value
	 */
	private function align_image( $instance ) {

		switch( $instance['align'] ) {
			case 'none':
				return 'float: none;';
				break;
			case 'left':
				return 'float: left;';
				break;
			case 'center':
				return 'margin: 0 auto;';
				break;
			case 'right':
				return 'float: right;';
				break;
		}
	}

	/**
	 * Returns correct string to use for link target attribute
	 *
	 * @param array $instance
	 * @return string Updated string value
	 */

	private function link_target( $instance ) {

		switch( $instance['target'] ) {
			case 'Stay in window':
				return '_self';
				break;
			case 'Open in new window':
				return '_blank';
				break;
		}
	}

	/**
	 * Create a label input in our widget form
	 *
	 * @param string $for The for attribute for the label input.
	 * @param string $text The label title.
	 */
	private function create_label( $for, $text ) {
		return '<label for="' . esc_attr( $for ) . '">' . esc_html__( $text, 'tcu_teaser_widget' ) . '</label>';
	}

	/**
	 * Create an input in our widget form
	 *
	 * @param string $type The type of input needed ('text', 'url')
	 * @param string $id The input id attribute.
	 * @param string $name The input name attribute.
	 * @param string $value The input value attribute.
	 * @param string $classname The classname of the input.
	 */
	private function create_input( $type, $id, $name, $value, $classname = 'widefat' ) {

		return '<input class="' . esc_attr( $classname ) . '" id="' . esc_attr( $id ) . '" name="'. esc_attr( $name ) .'" type="'. esc_attr( $type ) . '"  value="' . esc_attr( $value ) . '">';

	}
}
