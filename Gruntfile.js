/*eslint no-undef: "error"*/
/*eslint-env node*/
module.exports = function( grunt ) {
    grunt.initConfig( {

        // Let's minimize our JS files
        uglify: {

            // Front-end
            main: {
                src: 'js/tcu-image-button.js',
                dest: 'js/min/tcu-image-button.min.js'
            }
        },

        // This creates a clean WP plugin copy to use in production
        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        src: [ './*.php', 'classes/*.php', 'css/**.css', 'js/**', 'css/!*.map', './CHANGELOG.md', './README.md' ],
                        dest: 'dist/'
                    }
                ]
            }
        },

        // Let's zip up our /dist (production wordpress plugin)
        // Change version number in tcu_downloads.php
        compress: {
            main: {
                options: {
                    archive: 'tcu_image_buttons.1.4.zip'
                },
                files: [
                    {
                        expand: true,
                        cwd: 'dist/',
                        src: [ '**' ],
                        dest: 'tcu_image_buttons/'
                    }
                ]
            }
        }
    } );

    grunt.loadNpmTasks( 'grunt-contrib-uglify' );
    grunt.loadNpmTasks( 'grunt-contrib-copy' );
    grunt.loadNpmTasks( 'grunt-contrib-compress' );

    // min js and css
    grunt.registerTask( 'default', [ 'uglify' ] );

    // Copy to dist/
    grunt.registerTask( 'build', [ 'copy' ] );

    // Zip our clean directory to easily install in WP
    grunt.registerTask( 'zip', [ 'compress' ] );
};
